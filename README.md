# zb64

**zb64** is a simple cli tool to compress small files and strings 💌

## install

```bash
go get bitbucket.org/ai69/zb64
```

## usage

```bash
# compress
zb64 -e -r "YOUR_CONTENT...YOUR_CONTENT...YOUR_CONTENT...YOUR_CONTENT...YOUR_CONTENT"

# decompress
zb64 -d -r "ivQPDYp39vcLcfUL0dPTI5sLCAAA//8="
```
